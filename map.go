package crawler

import (
	"fmt"
	"sort"
	"strings"
)

// Map holds all the paths that where found starting at
// the baseURL. Paths has a path in each key and an array
// of all the paths that where reachable from it.
type Map struct {
	BaseURL string
	Paths   map[string][]string
}

// Flat returns a sorted array of all the urls that where
// found.
func (m Map) Flat() []string {
	paths := []string{}
	for base, childs := range m.Paths {
		paths = add(paths, base)
		for _, p := range childs {
			paths = add(paths, p)
		}
	}
	sort.Strings(paths)
	return paths
}

// String implements the Stringer interface.
func (m Map) String() string {
	s := "Paths at " + m.BaseURL
	for base, childs := range m.Paths {
		s = fmt.Sprintf("%s\n\tbase: %s", s, base)
		for _, v := range childs {
			s = fmt.Sprintf("%s\n\t - %s", s, v)
		}
	}
	return s
}

// add will append the value as long as it does not already
// exist in data.
func add(data []string, value string) []string {
	for _, v := range data {
		if v == value {
			return data
		}
	}
	return append(data, value)
}

// Tree is a hierarchical tree of the paths that were retrieved
// from a base url.
type Tree struct {
	Path   string `json:"path"`
	Childs []Tree `json:"childs"`
}

// BuildTree returns a tree built using the Map.
func BuildTree(m Map) Tree {
	t := Tree{Path: "/", Childs: []Tree{}}
	return buildTree(&m, t)
}

// buildTree recursive builds the hierarchical tree according
// to the depth of each path. Path / is considered to have depth == 0.
func buildTree(m *Map, t Tree) Tree {
	c := depthChilds(m.Paths[t.Path], t.Path)
	if len(c) == 0 {
		return t
	}
	for _, p := range c {
		tree := Tree{Path: p, Childs: []Tree{}}
		t.Childs = append(t.Childs, buildTree(m, tree))
	}
	return t
}

// depthChilds returns all the childs that have a higher depth than
// parent.
func depthChilds(childs []string, parent string) []string {
	c := []string{}
	depth := strings.Count(parent, "/")
	if parent == "/" {
		depth = 0
	}
	for _, v := range childs {
		if v == "/" {
			continue
		}
		if strings.Count(v, "/") > depth {
			c = append(c, v)
		}
	}
	return c
}
