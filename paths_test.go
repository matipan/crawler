package crawler

import (
	"strings"
	"testing"
)

func TestParsePath(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name      string
		host, url string
		expected  string
		isErr     bool
	}{
		{name: "emtpy parameters", host: "", url: "", expected: "", isErr: true},
		{name: "relative path", host: "test.com", url: "/hello/goodbye", expected: "/hello/goodbye", isErr: false},
		{name: "incorrect host", host: "test.com", url: "http://facebook.com/hello", expected: "", isErr: true},
		{name: "absolute with same host", host: "test.com", url: "http://test.com/hi", expected: "/hi", isErr: false},
		{name: "invalid scheme", host: "test.com", url: "ftp://nope", expected: "", isErr: true},
	}
	for _, c := range cases {
		t.Run(c.name, func(tt *testing.T) {
			url, err := parsePath(c.host, c.url)
			if err != nil && !c.isErr {
				tt.Errorf("expected %s but got error instead", c.expected)
				return
			}
			if url != c.expected {
				tt.Errorf("expected %s but got %s instead", c.expected, url)
			}
		})
	}
}

func TestCleanPath(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		path     string
		expected string
	}{
		{name: "empty path", path: "", expected: "/"},
		{name: "trailing ..", path: "../hi", expected: "/hi"},
		{name: "embedded ..", path: "/hi/../goodbye", expected: "/goodbye"},
		{name: "without starting /", path: "hello", expected: "/hello"},
		{name: "trailing /", path: "//hello", expected: "/hello"},
	}
	for _, c := range cases {
		t.Run(c.name, func(tt *testing.T) {
			if p := cleanPath(c.path); p != c.expected {
				t.Errorf("expected %s but got %s", c.expected, p)
			}
		})
	}
}

func TestExtractURLs(t *testing.T) {
	t.Parallel()

	body := `
	<html>
		<head>
			<title>Hello monzo!</title>
		</head>
		<body>
			<div>
				<span>
					<a href="/blog" />
					<a href="https://monzo.com">Monzo</a>
				</span>
				<a href="/blog" />
				<a href="/" />
				<a href="" />
				<a href"../blog"
				<a href="https://monzo.com">Monzo</a>
				<a href="../about" />
			</div>
			<a href="https://facebook.com/monzo" />
		</body>
	</html>
	`
	cw := New(InfiniteDepth)
	cw.host = "monzo.com"
	cw.paths = make(chan *path, 2)
	cw.extractURLs(strings.NewReader(body), "/")
	close(cw.paths)
	expected := []string{"/blog", "/about"}
	for i := 0; i < 2; i++ {
		if p := <-cw.paths; expected[i] != p.path {
			t.Errorf("expected %s but got %s", expected[i], p.path)
		}
	}
	if _, ok := <-cw.paths; ok {
		t.Errorf("channel should have no more data")
	}
}
