package crawler

import (
	"context"
	"testing"
)

func BenchmarkCrawl(b *testing.B) {
	baseURL := "http://monzo.com"
	cw := New(InfiniteDepth)
	_, err := cw.Crawl(context.TODO(), baseURL)
	if err != nil {
		b.Fatal("failed to crawl")
	}
}

func TestAPI(t *testing.T) {
	t.Parallel()

	baseURL := "http://example.org"
	cw := New(InfiniteDepth)
	siteMap, err := cw.Crawl(context.TODO(), baseURL)
	if err != nil {
		t.Fatalf("error while crawling: %s", err)
	}
	if len(siteMap.Paths) != 1 {
		t.Fatalf("expected 1 result but got %d", len(siteMap.Paths))
	}
	if _, err = cw.Crawl(context.TODO(), baseURL); err == nil {
		t.Fatalf("expected error since crawl can only be called once")
	}
	cw = New(InfiniteDepth)
	if _, err = cw.Crawl(context.TODO(), "/"); err == nil {
		t.Fatalf("expected error due to relative url")
	}
	if _, err = cw.Crawl(context.TODO(), "ftp://nope"); err == nil {
		t.Fatalf("expected error due to invalid scheme")
	}
}

func TestValidDepth(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name  string
		depth int
		path  string
		valid bool
	}{
		{name: "valid embedded depth", depth: 2, path: "/blog/tech", valid: true},
		{name: "valid single depth", depth: 1, path: "/", valid: true},
		{name: "invalid due to longer depth", depth: 2, path: "/blog/bye/hi", valid: false},
		{name: "valid with infinite depth", depth: InfiniteDepth, path: "/blog/hi/bye/almost/ok", valid: true},
	}

	for _, c := range cases {
		t.Run(c.name, func(tt *testing.T) {
			cw := New(c.depth)
			if cw.validDepth(c.path) != c.valid {
				tt.Errorf("expected depth check of %s to be %v", c.path, c.valid)
			}
		})
	}
}

func TestGet(t *testing.T) {
	t.Parallel()

	cw := &Crawler{
		scheme: "http",
		host:   "matiaspan.me",
		done:   make(chan empty, 1),
		paths:  make(chan *path, 5),
	}
	cw.get("/blog")
	<-cw.done
	<-cw.paths
	<-cw.paths
	close(cw.paths)
	if _, ok := <-cw.paths; ok {
		t.Errorf("should not be more results")
	}
}

func TestCrawl(t *testing.T) {
	t.Parallel()

	cw := &Crawler{
		depth:   2,
		scheme:  "http",
		host:    "matiaspan.me",
		visited: make(map[string][]string),
		paths:   make(chan *path, 10),
		done:    make(chan empty, 10),
	}
	cw.getURLs = func(p string) {
		cw.paths <- &path{
			parent: "/test",
			path:   "/too/long/path",
		}
		if p == "/test" {
			cw.paths <- &path{
				parent: "/test",
				path:   "/",
			}
		} else {
			cw.paths <- &path{
				parent: "/",
				path:   "/test",
			}
		}
		cw.done <- empty{}
	}
	cw.crawl(context.TODO(), "/")
	if _, ok := cw.visited["/"]; !ok {
		t.Fatalf("http://matiaspan.me should be visited")
	}
	if _, ok := cw.visited["/test"]; !ok {
		t.Fatalf("http://matiaspan.me/test should be visited")
	}
}

func TestCrawlContext(t *testing.T) {
	t.Parallel()

	cw := &Crawler{
		depth:   1,
		scheme:  "http",
		host:    "matiaspan.me",
		visited: make(map[string][]string),
		paths:   make(chan *path, 10),
		done:    make(chan empty, 10),
	}
	cw.getURLs = func(baseURL string) {
		cw.paths <- &path{
			parent: "/",
			path:   "/test",
		}
		cw.done <- empty{}
	}
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	cw.crawl(ctx, "/")
	if _, ok := cw.visited["/"]; !ok {
		t.Fatalf("http://matiaspan.me should be visited")
	}
	if _, ok := cw.visited["/test"]; ok {
		t.Fatalf("http://matiaspan.me/test should not be visited")
	}
}
