package crawler

import (
	"errors"
	"io"
	"net/url"
	"path/filepath"

	"golang.org/x/net/html"
)

type path struct {
	parent, path string
}

func (c *Crawler) extractURLs(r io.Reader, parentPath string) {
	paths := map[string]empty{}
	paths[parentPath] = empty{}
	z := html.NewTokenizer(r)
	for {
		t := z.Next()
		switch t {
		case html.ErrorToken:
			return
		case html.StartTagToken, html.EndTagToken, html.SelfClosingTagToken:
			token := z.Token()
			if token.Data != "a" {
				break
			}
			for _, attr := range token.Attr {
				if attr.Key != "href" {
					continue
				}
				if p, err := parsePath(c.host, attr.Val); err == nil {
					if _, ok := paths[p]; ok {
						continue
					}
					paths[p] = empty{}
					c.paths <- &path{parent: parentPath, path: p}
				}
			}
		}
	}
}

// parsePath returns a clean path of purl as long as the host
// of purl is the same as host or purl is relative.
func parsePath(host, purl string) (string, error) {
	if host == "" || purl == "" {
		return "", errors.New("invalid parameters")
	}
	u, err := url.Parse(purl)
	if err != nil {
		return "", errors.New("failed to parse URL")
	}
	if u.IsAbs() && u.Hostname() != host {
		return "", errors.New("invalid host")
	}
	if u.Scheme != schemeHTTP && u.Scheme != schemeHTTPS && u.Scheme != "" {
		return "", errors.New("invalid scheme")
	}
	return cleanPath(u.Path + u.Query().Encode()), nil
}

// cleanPath returns path parsed to be a valid http path.
// This function makes the program unix-compatible only.
func cleanPath(path string) string {
	return filepath.Join("/", path)
}
