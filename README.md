# Usage
From the root run:
```
go run cmd/main.go https://monzo.com >> monzo.txt
```
In `monzo.txt` you'll have all the URLs that were processed and the time it took to do it.

### As library
Create a new crawler with the desired depth by using the method `crawler.New`. After that you can call `Crawl` with the context used for cancellation and the base url that will be used. The URL has to be absolute and the only supported schemes are `http` and `https`. Example:
```go
func main() {
	cw := crawler.New(crawler.InfiniteDepth)
	siteMap, err := cw.Crawl(context.TODO(), "https://monzo.com")
	if err != nil {
		log.Fatalf("uh-oh")
	}
	// Use siteMap
}
```
The structure returned by `Crawl` is called a `crawler.Map`. `Map` holds the base url used when calling `Crawl` and a map where each key is a path and each value is an array of all the paths that are reachable from it.  
If you'd like to have a simple array of all the unique paths that where reached you can call the method `Flat` on your `Map`. Or if you'd like a hierarchical tree of paths where the depth of the paths grows as you go further down the tree you can call `crawler.BuildTree(Map)`. Example:
```go
// print all the reachable paths from each path, something like this:
// 	Paths at https://monzo.com
//	base: /
//	 - /about
//	 - /blog
//	 - /community
//	 - /faq
//	 ...
//	base: /blog/2018/01/26/upgrade-credit-score
//	 ...
//	 - /features/overdrafts
//	 - /blog/authors/beatrice-borbon
//	 - /blog/2017/12/18/why-upgrade
//	 - /cdn-cgi/l/email-protection
//	 ...
//	base: /university/lost-or-stolen-bank-card
//	 - /blog/2018/06/11/lost-or-stolen-card
//	 ...
fmt.Println(siteMap)

// print all unique paths that were found
fmt.Println(siteMap.Flat())

// Tree can be useful to know how many clicks does a user need to
// to make in order to get to a particular path.
tree := crawler.BuildTree(siteMap)
```

# Benchmarks
I've only tested with up to ~6000 URLs. Here are a few results:
```
Base: http://matiaspan.me
Processed 3 URLs in 4 seconds
-----------------------------
Base: https://monzo.com
Processed 466 URLs in 8 seconds
-----------------------------
Base: https://godoc.org
Processed 5839 URLs in 52 seconds
```
Times will vary depending on how good your internet is and how far the destination is.  
But you can see that as the throughtput gets higher the amount of time it takes does not grow linearly. If it did then the 5839 URLs would have taken ~100 seconds.
