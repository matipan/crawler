package crawler

import "testing"

func TestFlat(t *testing.T) {
	t.Parallel()

	siteMap := Map{
		BaseURL: "http://matiaspan.me",
		Paths: map[string][]string{
			"/":     []string{"/test", "/blog"},
			"/test": []string{"/blog", "/"},
			"/blog": []string{"/"},
		},
	}
	expected := []string{"/", "/blog", "/test"}
	flat := siteMap.Flat()
	if len(flat) != 3 {
		t.Fatalf("expected to have 3 urls, instead got: %d", len(flat))
	}
	for k, v := range flat {
		if expected[k] != v {
			t.Fatalf("expected to be ordered. Got %s expected %s", v, expected[k])
		}
	}
}

func TestAdd(t *testing.T) {
	t.Parallel()

	data := []string{"hello", "goodbye", "monzo"}
	cases := []struct {
		data      []string
		value     string
		shouldAdd bool
	}{
		{data: data, value: "goodbye", shouldAdd: false},
		{data: data, value: "yup", shouldAdd: true},
	}
	for _, c := range cases {
		res := add(c.data, c.value)
		if c.shouldAdd && len(res) == len(c.data) {
			t.Fatal("did not add a value that should be added")
		}
		if !c.shouldAdd && len(res) > len(c.data) {
			t.Fatal("added a value that should not be added")
		}
	}
}

func TestBuildTree(t *testing.T) {
	m := Map{
		BaseURL: "http://matiaspan.me",
		Paths: map[string][]string{
			"/":     []string{"/blog"},
			"/blog": []string{"/", "/blog/building-basic-blog-in-golang"},
			"/blog/building-basic-blog-in-golang": []string{"/blog", "/"},
		},
	}
	tree := BuildTree(m)
	if tree.Path != "/" {
		t.Fatalf("first node should be /")
	}
	if len(tree.Childs) != 1 {
		t.Fatalf("expected only 1 child at root but got %d", len(tree.Childs))
	}
	t1 := tree.Childs[0]
	if t1.Path != "/blog" {
		t.Fatalf("/blog should be at first level, got: %s", t1.Path)
	}
	if len(t1.Childs) != 1 {
		t.Fatalf("expected only 1 child at first level but got %d", len(t1.Childs))
	}
	t2 := t1.Childs[0]
	if t2.Path != "/blog/building-basic-blog-in-golang" {
		t.Fatalf("/blog/building-basic-blog-in-golang should be at second level, got: %s", t2.Path)
	}
	if len(t2.Childs) != 0 {
		t.Fatalf("expected no childs at second level but got: %d", len(t2.Childs))
	}
}
