package crawler

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const (
	// InfiniteDepth is the depth used to crawl the entire website.
	InfiniteDepth = 0

	schemeHTTP  = "http"
	schemeHTTPS = "https"
)

type empty struct{}

// Crawler has a depth that it's used to know when to stop crawling.
type Crawler struct {
	scheme, host        string
	depth               int
	requests, responses int
	run                 bool
	visited             map[string][]string

	paths chan *path
	done  chan empty

	getURLs func(baseURL string)
}

// New returns a Crawler with the specified depth. If depth is < 0
// then it will be InfiniteDepth.
func New(depth int) *Crawler {
	if depth < 0 {
		depth = InfiniteDepth
	}
	return &Crawler{
		depth: depth,
	}
}

// Crawl crawls baseURL until it reaches to the pre-configured depth.
func (c *Crawler) Crawl(ctx context.Context, baseURL string) (Map, error) {
	if c.run {
		return Map{}, errors.New("crawl can only be called once")
	}
	u, err := url.Parse(baseURL)
	if err != nil {
		return Map{}, err
	}
	if !u.IsAbs() {
		return Map{}, errors.New("absolute url is expected")
	}
	if u.Scheme != schemeHTTP && u.Scheme != schemeHTTPS {
		return Map{}, errors.New("unsupported scheme")
	}
	c.host, c.scheme = u.Hostname(), u.Scheme
	c.run = true
	c.getURLs = c.get
	c.paths = make(chan *path, 60)
	c.done = make(chan empty)
	c.visited = make(map[string][]string)
	return c.crawl(ctx, cleanPath(u.Path))
}

func (c *Crawler) crawl(ctx context.Context, basePath string) (Map, error) {
	c.visited[basePath] = []string{}
	c.requests, c.responses = 1, 0
	go c.getURLs(basePath)
	for {
		select {
		case p := <-c.paths:
			c.visited[p.parent] = append(c.visited[p.parent], p.path)
			if _, ok := c.visited[p.path]; ok {
				continue
			}
			if !c.validDepth(p.path) {
				continue
			}
			c.visited[p.path] = []string{}
			c.requests++
			go c.getURLs(p.path)
		case <-ctx.Done():
			defer close(c.paths)
			defer close(c.done)
			for p := range c.paths {
				c.responses++
				c.visited[p.parent] = append(c.visited[p.parent], p.path)
				if c.responses == c.requests {
					return Map{
						BaseURL: fmt.Sprintf("%s://%s", c.scheme, c.host),
						Paths:   c.visited,
					}, errors.New("context was cancelled")
				}
			}
		default:
			select {
			case <-c.done:
				c.responses++
				if c.responses == c.requests {
					return Map{
						BaseURL: fmt.Sprintf("%s://%s", c.scheme, c.host),
						Paths:   c.visited,
					}, nil
				}
			default:
				break
			}
		}
	}
}

func (c *Crawler) get(path string) {
	w, err := http.Get(fmt.Sprintf("%s://%s%s", c.scheme, c.host, path))
	if err == nil && validContentType(w) {
	    defer w.Body.Close()
		c.extractURLs(w.Body, path)
	}
	c.done <- empty{}
}

func (c *Crawler) validDepth(baseURL string) bool {
	if c.depth == InfiniteDepth {
		return true
	}
	return strings.Count(baseURL, "/") <= c.depth
}

func validContentType(w *http.Response) bool {
	ct := w.Header.Get("Content-type")
	return strings.Contains(ct, "html")
}
