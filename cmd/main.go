package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/matipan/crawler"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("crawler expects one argument: go run cmd/main.go https://monzo.com")
		os.Exit(1)
	}
	cw := crawler.New(crawler.InfiniteDepth)
	before := time.Now()
	fmt.Println("Crawling, hang tight...")
	siteMap, err := cw.Crawl(context.TODO(), os.Args[1])
	if err != nil {
		log.Fatalf("failed while crawling: %s", err)
	}
	now := time.Now()
	urls := siteMap.Flat()
	fmt.Println(siteMap)
	fmt.Printf("Processed %d URLs in %d seconds\n", len(urls), now.Unix()-before.Unix())
}
